
Each meme is represented as a Tree of different Nodes. The hierarchy is set up as follows:

MemeNode:
	BoolExp: These Nodes contain Boolean expressions
		Statement: This is probably the highest level class used in Meme trees. It uses a comparator operator to compare two NumExp.
		BoolProposition: Indicates a description of something with a Boolean value.
		BoolBinExp: Contains two other BoolExp and compares them with AND or OR.
		NotExp: Used to negate another BoolExp.
		BoolLiteral: A Boolean value.
	NumExp: These Nodes contain Numerical expressions
		Probability: Used to indicate a probability in a statement. Takes as input an argument and a condition, both BoolExp, indicating that the probability is that the argument occurs given the condition.
		NumProposition: Indicates a description of something with a numerical value.
		NumBinExp: Contains two other NumExp and adds or subtracts them.
		NumLiteral: A number. Can be set with an is_probability descriptor.

For example, the meme that international applications are down at 40% of universities would be represented by the following structure:
    Statement( # The below probability is equal to 0.4
        CompOp.EQUAL,
        Probability( # The probability of the below given we are looking at one school
            Statement( # The amount that applications are down is greater than 0
                CompOp.GREATER,
                NumProposition("The amount applications are down"),
                NumLiteral(0.0)
            ),
            BoolProposition("We are analyzing one school")
        ),
        NumLiteral(0.4)
    )

Each Meme object is made by adding MemeNode trees to it.
