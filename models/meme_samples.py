from models.meme import Meme, Probability, BoolProposition, Statement, CompOp,\
    NumLiteral, NumProposition, BoolBinExp, BoolBinOp, NotExp, BoolLiteral
from models.mutations import *

NONE = NumLiteral(0.0)
SMALL = NumLiteral(0.1)
MEDIUM = NumLiteral(0.5)
LARGE = NumLiteral(0.9)
CERTAIN = NumLiteral(1.0)

"""Mac and linux are unlikely to get viruses"""
mac_virus_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Getting a virus"),
            BoolProposition("Is mac or linux")
        ),
        SMALL
    ))

"""You have a chance of experiencing a side effect after taking a drug,
but if you've taken the drug you're probably sick"""
has_taken_drug_prop = BoolProposition("Has taken drug")
side_effect_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Experience Side Effect"),
            has_taken_drug_prop
        ),
        MEDIUM
    ))\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Is sick"),
            has_taken_drug_prop
        ),
        CERTAIN
    ))

"""The earth will only rise in temp a few degrees"""
earth_temp_rise_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        NumProposition("The earth's temperature rise"),
        NumLiteral(2.0, False)
    ))

"""The earth has experienced higher CO2 levels before,
but never in so short a time"""
earth_co2_timeline_meme = Meme()\
    .add(BoolProposition(
        "The earth has experienced higher CO2 levels in the past"
    ))\
    .add(BoolProposition(
        "The earth has never experienced such a sudden rise in CO2 levels"
    ))

"""If you're within 3 months of recovering from COVID, you're not susceptible to reinfection"""
covid_reinfection_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Getting reinfected with covid"),
            Statement(
                CompOp.LESS,
                NumProposition("Time since recovery in months"),
                NumLiteral(3.0, False)
            )
        ),
        NONE
    ))

"""International applications are down at 40% of schools"""
international_applications_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            Statement(
                CompOp.GREATER,
                NumProposition("The amount applications are down"),
                NONE
            ),
            BoolProposition("We are analyzing one school")
        ),
        NumLiteral(0.4)
    ))\
    .add_positive_mutations([])\
    .add_negative_mutations([Exaggerator,\
                             LossOfContext,\
                             ReverseImplication,\
                             LossOfCondition,\
                             FaultyNegation,\
                             CommonCauseFallacy,\
                             ProbabilityCollapse])

"""Having cancer increases risk of low cholesterol (idk if that's actually true)"""
cancer_cholesterol_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Having low cholesterol"),
            BoolProposition("Having cancer")
        ),
        LARGE
    ))

"""Ice cream sales and drowning correlate, because they're caused by summer"""
summer_prop = BoolProposition("It is summer")
ice_cream_drowning_meme = Meme()\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Ice cream sales are up"),
            summer_prop
        ),
        LARGE
    ))\
    .add(Statement(
        CompOp.EQUAL,
        Probability(
            BoolProposition("Drowning is up"),
            summer_prop
        ),
        LARGE
    ))

all_memes = [
    mac_virus_meme,
    side_effect_meme,
    earth_temp_rise_meme,
    earth_co2_timeline_meme,
    covid_reinfection_meme,
    international_applications_meme,
    cancer_cholesterol_meme,
    ice_cream_drowning_meme
]