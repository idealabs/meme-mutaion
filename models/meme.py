from abc import abstractmethod
from enum import Enum
from typing import Optional, List
import numpy as np

class MemeNode:
    @abstractmethod
    def visit(self, mutation: 'Mutation') -> Optional['MemeNode']:
        return mutation.mutate(self)

    @abstractmethod
    def get_complexity(self) -> float:
        pass

    @abstractmethod
    def __repr__(self) -> str:
        pass

class Mutation:
    @abstractmethod
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        pass

class NumExp(MemeNode):
    pass

class NumProposition(NumExp):
    def __init__(self, name: str) -> None:
        super().__init__()
        self.name = name

    def get_complexity(self) -> float:
        return 1

    def __repr__(self):
        return f'"{self.name}"'

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, NumProposition):
            return False
        return self.name == __o.name

class NumBinOp(Enum):
    PLUS = '+'
    MINUS = '-'

def binaryVisit(self: MemeNode, mutation: Mutation, type) -> Optional[MemeNode]:
    mutated_self = mutation.mutate(self)
    if (mutated_self is not None):
        return mutated_self

    mutated_lhs = self.lhs.visit(mutation)
    mutated_rhs = self.rhs.visit(mutation)
    if mutated_lhs is not None or mutated_rhs is not None:
        return type(self.op, mutated_lhs or self.lhs, mutated_rhs or self.rhs)
    else:
        return None

def binaryEquals(self, other, type):
    if not isinstance(other, type):
        return False
    return self.lhs == other.lhs and self.rhs == other.rhs and self.op is other.op

class NumBinExp(NumExp):
    def __init__(self, op: NumBinOp, lhs: NumExp, rhs: NumExp) -> None:
        super().__init__()
        self.op = op
        self.lhs = lhs
        self.rhs = rhs

    def visit(self, mutation: Mutation) -> Optional[MemeNode]:
        return binaryVisit(self, mutation, NumBinExp)

    def get_complexity(self) -> float:
        return self.lhs.get_complexity() + self.rhs.get_complexity()

    def __repr__(self):
        return f'({self.lhs} {self.op.value} {self.rhs})'

    def __eq__(self, __o: object) -> bool:
        return binaryEquals(self, __o, NumBinExp)

class NumLiteral(NumExp):
    def __init__(self, value: float, is_probability = True) -> None:
        super().__init__()
        self.value = value
        self.is_probability = is_probability

    def get_complexity(self) -> float:
        return 1

    def __repr__(self):
        return str(self.value)

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, NumLiteral):
            return False
        return self.value == __o.value

class BoolExp(MemeNode):
    negated = False

class BoolProposition(BoolExp):
    def __init__(self, name: str) -> None:
        super().__init__()
        self.name = name

    def get_complexity(self) -> float:
        return 1

    def __repr__(self) -> str:
        negative = 'NOT ' if self.negated else ''
        return f'{negative}"{self.name}"'

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, BoolProposition):
            return False
        return self.name == __o.name

    def __hash__(self) -> int:
        return hash(self.name)

class BoolBinOp(Enum):
    AND = '&&'
    OR = '||'

class BoolBinExp(BoolExp):
    def __init__(self, op: BoolBinOp, lhs: BoolExp, rhs: BoolExp) -> None:
        super().__init__()
        self.op = op
        self.lhs = lhs
        self.rhs = rhs

    def visit(self, mutation: 'Mutation') -> Optional[MemeNode]:
        return binaryVisit(self, mutation, BoolBinExp)

    def get_complexity(self) -> float:
        return self.lhs.get_complexity() + self.rhs.get_complexity()

    def __repr__(self) -> str:
        return f'({self.lhs} {self.op.value} {self.rhs})'

    def __eq__(self, __o: object) -> bool:
        return binaryEquals(self, __o, BoolBinExp)

class NotExp(BoolExp):
    def __init__(self, exp: BoolExp) -> None:
        super().__init__()
        self.exp = exp
    
    def get_complexity(self) -> float:
        return 1 + self.exp.get_complexity()
    
    def __repr__(self) -> str:
        return f'!({self.exp})'

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, NotExp):
            return False
        return self.exp == __o.exp

class BoolLiteral(BoolExp):
    def __init__(self, value: bool) -> None:
        super().__init__()
        self.value = value

    def get_complexity(self) -> float:
        return 1

    def __repr__(self):
        negative = 'NOT' if self.negated else ''
        return f'{negative}{self.value}'

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, BoolLiteral):
            return False
        return self.value == __o.value

class CompOp(Enum):
    EQUAL = '=='
    LESS = '<'
    GREATER = '>'
    LTE = '<='
    GTE = '>='

class Statement(BoolExp):
    def __init__(self, op: CompOp, lhs: NumExp, rhs: NumExp) -> None:
        super().__init__()
        self.op = op
        self.lhs = lhs
        self.rhs = rhs

    def visit(self, mutation: 'Mutation') -> Optional[MemeNode]:
        return binaryVisit(self, mutation, Statement)

    def get_complexity(self) -> float:
        return self.lhs.get_complexity() + self.rhs.get_complexity()

    def __repr__(self) -> str:
        return f'({self.lhs} {self.op.value} {self.rhs})'

    def __eq__(self, __o: object) -> bool:
        return binaryEquals(self, __o, Statement)

class Probability(NumExp):
    def __init__(self, argument: BoolExp, condition: Optional[BoolExp] = None) -> None:
        super().__init__()
        self.argument = argument
        self.condition = condition

    def visit(self, mutation: 'Mutation') -> Optional[MemeNode]:
        mutated_self = mutation.mutate(self)
        if (mutated_self is not None):
            return mutated_self

        mutated_arg = self.argument.visit(mutation)
        mutated_cond = self.condition and self.condition.visit(mutation)

        if (mutated_arg is not None or mutated_cond is not None):
            return Probability(mutated_arg or self.argument, mutated_cond or self.condition)
        else:
            return None

    def get_complexity(self) -> float:
        GIVEN_MULTIPLIER = 2.0

        return self.argument.get_complexity() + self.condition.get_complexity() * GIVEN_MULTIPLIER if self.condition is not None else 1

    def __repr__(self) -> str:
        return f'P({self.argument}' + (f' | {self.condition})' if self.condition is not None else ')')

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, Probability):
            return False
        return self.argument == __o.argument and self.condition == __o.condition

class Meme(MemeNode):
    def __init__(self) -> None:
        self.facts = []
        self.positive_mutations = []
        self.negative_mutations = []
    
    def add_positive_mutations(self, mutations: List[Mutation]):
        self.positive_mutations.extend(mutations)
        return self

    def add_negative_mutations(self, mutations: List[Mutation]):
        self.negative_mutations.extend(mutations)
        return self

    def add(self, fact: BoolExp):
        self.facts.append(fact)
        return self

    def visit(self, mutation: 'Mutation') -> Optional[MemeNode]:
        mutated_self = mutation.mutate(self)
        if (mutated_self is not None):
            return mutated_self

        mutated_facts = [fact.visit(mutation) for fact in self.facts]
        if (any(mutated_facts)):
            mutated_meme = Meme()
            for (i, fact) in enumerate(mutated_facts):
                mutated_meme.add(fact or self.facts[i])
            return mutated_meme
        else:
            return None

    def get_complexity(self) -> float:
        return sum([fact.get_complexity() for fact in self.facts])

    def get_opinion_vector(self):

        opinion_vector = []

        for fact in self.facts:
            if isinstance(fact, Statement): 

                prob = None
                val = None
                if isinstance(fact.lhs, Probability) and isinstance(fact.rhs, NumLiteral):
                    prob = fact.lhs
                    val = fact.rhs
                elif isinstance(fact.rhs, Probability) and isinstance(fact.lhs, NumLiteral):
                    prob = fact.rhs
                    val = fact.lhs

                op_given = val.value * 2 - 1
                op_not_given = op_given

                if prob.condition is not None:
                    if prob.condition.negated:
                        op_given = None
                    else:
                        op_not_given = None

                opinion_vector.append(op_given)
                opinion_vector.append(op_not_given)
                
        if len(opinion_vector) == 0: 
            opinion_vector.append(0)
        
        return np.array(opinion_vector, dtype=np.float32)

    def __repr__(self) -> str:
        val = f'Meme:\n'
        for fact in self.facts:
            val += f'\t{fact}\n'
        return val

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, Meme):
            return False
        return all([fact == __o.facts[i] for (i, fact) in enumerate(self.facts)])

    def __hash__(self) -> int:
        return hash('\0'.join([str(fact) for fact in self.facts]))