import math
import random
from typing import Optional
from models.meme import MemeNode, Mutation, NumLiteral, CompOp, Meme, NotExp, Probability, Statement

class Exaggerator(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, NumLiteral) and node.is_probability:
            value = node.value
            if value == 0.0 or value == 1.0: # these will be out of domain
                return value

            return NumLiteral(1 / (1 + math.pow(value / (1 - value), -2)))
        
class Mitigator(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, NumLiteral) and node.is_probability:
            value = node.value
            if value == 0.0 or value == 1.0: # these will be out of domain
                return value

            return NumLiteral((value - math.sqrt(value - value**2))/(2 * value - 1))

class LossOfContext(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Meme) and len(node.facts) > 1:
            fact_to_drop = random.randint(0, len(node.facts) - 1)
            facts = [fact for i,fact in enumerate(node.facts) if i != fact_to_drop]
            meme = Meme()
            for fact in facts:
                meme.add(fact)
            return meme

class ReverseImplication(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Probability) and node.condition is not None:
            return Probability(node.condition, node.argument)

class LossOfCondition(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Probability) and node.condition is not None:
            return Probability(node.argument)

class FaultyNegation(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Probability) and isinstance(node.condition, NotExp):
            return Probability(NotExp(node.argument), node.condition.exp)

# This is a very specific example, can be generalized further
class CommonCauseFallacy(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Meme) and len(node.facts) > 1:
            for (i, fact_a) in enumerate(node.facts):
                for fact_b in node.facts[i + 1:]:
                    if isinstance(fact_a, Statement) and isinstance(fact_b, Statement) and fact_a.op == fact_b.op:
                        if fact_a.rhs == fact_b.rhs and isinstance(fact_a.lhs, Probability)\
                                and isinstance(fact_b.lhs, Probability) and fact_a.lhs.condition == fact_b.lhs.condition:
                            meme = Meme()
                            for fact in [fact for fact in fact if fact != fact_a and fact != fact_b]:
                                meme.add(fact)
                            meme.add(Statement(
                                fact_a.op,
                                Probability(
                                    fact_a.lhs.argument,
                                    fact_b.lhs.argument
                                ),
                                fact_a.rhs
                            ))
                            return meme

# specific instance, can be generalized  
class ProbabilityCollapse(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Probability) and isinstance(node.argument, Statement)\
                and node.argument.op is CompOp.GREATER and isinstance(node.argument.rhs, NumLiteral)\
                and node.argument.rhs.value == 0.0:
            return node.argument.lhs

class Transversion(Mutation):
    def mutate(self, node: MemeNode) -> Optional[MemeNode]:
        if isinstance(node, Statement):
            prob = None
            val = None
            if isinstance(node.lhs, Probability) and isinstance(node.rhs, NumLiteral):
                prob = node.lhs
                val = node.rhs
            elif isinstance(node.rhs, Probability) and isinstance(node.lhs, NumLiteral):
                prob = node.rhs
                val = node.lhs
            
            if prob is None or val is None or prob.condition is None:
                return None
            
            new_value = NumLiteral(1 - val.value)
            new_condition = prob.condition
            new_condition.negated = True
            new_probability = Probability(prob.argument, new_condition)

            if isinstance(node.lhs, Probability) and isinstance(node.rhs, NumLiteral):
                new_statement = Statement(node.op, new_probability, new_value)
            else:
                new_statement = Statement(node.op, new_value, new_probability)
                
            return new_statement

all_mutations = [
    Exaggerator,
    Mitigator,
    # LossOfContext,
    # ReverseImplication,
    LossOfCondition,
    # FaultyNegation,
    # CommonCauseFallacy,
    # ProbabilityCollapse,
    Transversion
]
