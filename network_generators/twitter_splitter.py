import sys

(_, chunk_size_str, file_path, out_dir) = sys.argv
chunk_size = int(chunk_size_str)

file = open(file_path, 'r')

line_counter = 0
chunk_counter = 0
for line in file:
    if line_counter == 0:
        out_file = open(out_dir  + '/' + str(chunk_counter) + '.txt', 'w')
        print(str(chunk_counter * chunk_size) + ' lines complete')
    out_file.write(line)
    line_counter += 1
    if line_counter == chunk_size:
        chunk_counter += 1
        line_counter = 0
