import os
import sys
import time

#
(_, depth_str, start_node_str, in_dir, skip_str) = sys.argv
depth_limit = int(depth_str)
start_node = int(start_node_str)
skip = int(skip_str)

file_paths = list(map(lambda x: in_dir + x, os.listdir(in_dir)))
num_files = len(file_paths)
nodes_to_children = {
    start_node: set()
}

def write_out(depth):
    out_file = open(f'./out-{depth}.txt', 'w')
    for (followee, followers) in nodes_to_children.items():
        if (len(followers) > 0):
            out_file.write(f'{str(followee)} {" ".join(map(str, followers))}\n')

time_start = time.perf_counter()
for i in range(depth_limit):
    nodes = set(nodes_to_children.keys())
    print(f'nodes: {len(nodes)}')
    file_counter = 0
    for file_path in file_paths:
        time_elapsed = time.perf_counter() - time_start
        print(f'{str(i + 1)} of {str(depth_limit)}: {file_counter}/{num_files} in {int(time_elapsed)}s')
        file = open(file_path, 'r')
        line_counter = -1
        for line in file:
            line_counter += 1
            if (line_counter % skip != 0):
                continue
            (followee, follower) = map(int, line.split(' '))
            if (follower in nodes):
                if (followee not in nodes_to_children):
                    nodes_to_children[followee] = set()

                nodes_to_children[follower].add(followee)
        file_counter += 1
    write_out(i)
